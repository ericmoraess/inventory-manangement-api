
package br.com.ericmoraess;

import java.time.LocalDate;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;

import br.com.ericmoraess.config.DepreciationProperties;
import br.com.ericmoraess.config.FileStorageProperties;
import br.com.ericmoraess.entity.Equipment;
import br.com.ericmoraess.entity.IEntity;
import br.com.ericmoraess.entity.Model;
import br.com.ericmoraess.entity.Type;
import br.com.ericmoraess.service.AulaService;
import br.com.ericmoraess.service.EquipmentService;
import br.com.ericmoraess.service.IServiceCRUD;
import br.com.ericmoraess.service.ModelService;
import br.com.ericmoraess.service.TypeService;

@SpringBootApplication
@RestController
@EnableConfigurationProperties({
	FileStorageProperties.class,
	DepreciationProperties.class
})
public class MainApplication {

	final public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}
	
	@Bean
    CommandLineRunner init(EquipmentService equipmentService, AulaService aulaService, TypeService typeService, ModelService modelService) {
        return args -> {
        	
//        	Tarefa a = new Tarefa();
//        	a.setDescricao("alguma coisa");
//        	Aula aa = new Aula();
//        	ArrayList<Tarefa> aas = new ArrayList();
//        	aas.add(a);
//        	aa.setTarefas(aas);
//        	
//        	aulaService.save(aa);
        	
        	Type type = new Type();
        	type.setCode("C");
        	type.setName("CARRO");
        	
        	
        	Model model = new Model();
        	model.setCode("VS");
        	model.setName("VERSA");
        	model.setType(type);
            
            typeService.save(type);
            modelService.save(model);
            
//            model = (Model) getAttatchedEntity(model, modelService);
            
            Equipment equipment = new Equipment();
            equipment.setCode("CVS");
//            equipment.setModel(model);
//            equipment.setType(type);
            equipment.setAcquisitionDate( LocalDate.now() );
            equipment.setValueAcquisition(new Double("150.98"));
            equipment.setUrl("https://abrilsuperinteressante.files.wordpress.com/2018/05/filhotes-de-cachorro-alcanc3a7am-o-c3a1pice-de-fofura-com-8-semanas1.png");
            
            equipmentService.save(equipment);
            
           
        };
    }
	
	private static IEntity getAttatchedEntity(IEntity e, IServiceCRUD service) {
		IEntity attatched = ((IServiceCRUD) service).searchByName(e.getName());

		if (attatched == null)
			attatched = ((IServiceCRUD) service).save(e);

		return attatched;
	}
}