package br.com.ericmoraess.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
public class DepreciationProperties {
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Bean
	@ConfigurationProperties(prefix = "depreciation")
	public DepreciationProperties depreciationValueProperties() {
		return new DepreciationProperties();
	}

}
