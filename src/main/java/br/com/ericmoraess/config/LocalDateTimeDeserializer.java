package br.com.ericmoraess.config;

import java.io.IOException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDate>{

    @Override
    public LocalDate deserialize(JsonParser arg0, DeserializationContext arg1) throws IOException {
    	
    	YearMonth yearMonth = YearMonth.parse(arg0.getText(),DateTimeFormatter.ofPattern("MM/yyyy"));
    	LocalDate firstDay = yearMonth.atDay(1);
    	
        return firstDay;
    }

}
