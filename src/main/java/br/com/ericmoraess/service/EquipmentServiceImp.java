package br.com.ericmoraess.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ericmoraess.entity.IEntity;
import br.com.ericmoraess.entity.Equipment;
import br.com.ericmoraess.repository.EquipmentRepository;

@Service
@Transactional
public class EquipmentServiceImp implements EquipmentService {

	@Autowired
	EquipmentRepository repository;

	@Override
	public <T> List<T> findAll() {
		return (List<T>) repository.findAll();
	}

	@Override
	public <T> Optional<T> findById(Long id) {
		return (Optional<T>) repository.findById(id);
	}

	@Override
	public <T> void delete(T entity) {
		repository.delete((Equipment) entity);
	}

	@Override
	public <S extends T, T> S save(Object entity) {
		Equipment m = (Equipment) entity;
		repository.save(m);
		return (S) m;
	}

	@Override
	public IEntity searchByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
