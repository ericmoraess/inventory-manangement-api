package br.com.ericmoraess.service;

import br.com.ericmoraess.entity.Model;

public interface ModelService extends IServiceCRUD {
	public Model searchByName(String name);
}
