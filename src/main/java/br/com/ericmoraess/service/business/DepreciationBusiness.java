package br.com.ericmoraess.service.business;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import br.com.ericmoraess.config.DepreciationProperties;
import br.com.ericmoraess.entity.Equipment;
import br.com.ericmoraess.entity.IEntity;

@Service
public class DepreciationBusiness implements ServiceBussines {

	@Autowired
	@Qualifier("depreciationValueProperties")
	private DepreciationProperties depreciationProperties;
	
	/*
	 * Este valor deve ser exibido considerando uma depreciação de X% ​ao mês​,
	 * relativo ao mês e ano de aquisição até a data atual.
	 */
	@Override
	public IEntity apply(IEntity entity) {

		Equipment e = (Equipment) entity;

		BigDecimal percentualValueDepreciation = new BigDecimal(depreciationProperties.getValue());
		percentualValueDepreciation = percentualValueDepreciation.divide(new BigDecimal("100"));
		
		LocalDate aquisitionDate = e.getAcquisitionDate();
		LocalDate now = LocalDate.now();
		
		Period intervalPeriod = Period.between(aquisitionDate, now);
		int monthsDiference = intervalPeriod.getDays() + intervalPeriod.getMonths();
		
		BigDecimal newValue = new BigDecimal(e.getValueAcquisition());
		for (int mes = 0; mes < monthsDiference; mes++) {
			newValue = newValue.subtract(newValue.multiply(percentualValueDepreciation));
			System.out.println("mes:" + mes + "valor:" + newValue);
		}
		
		e.setDepreciatedValue(Double.valueOf( newValue.toPlainString() ));

		return e;
	}

}
