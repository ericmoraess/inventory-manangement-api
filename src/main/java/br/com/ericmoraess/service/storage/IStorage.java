package br.com.ericmoraess.service.storage;

import org.springframework.web.multipart.MultipartFile;

/*
 * Interface to abstract storage process
 */
public interface IStorage {
	public String storeToFile(MultipartFile file);
}
