package br.com.ericmoraess.service;

import java.util.List;
import java.util.Optional;

import br.com.ericmoraess.entity.IEntity;

public interface IServiceCRUD {
	public <T> List<T> findAll();
	public <T> Optional<T> findById(Long id);
	public <S extends T, T> S save(Object entity);
	public <T> void delete(T entity);
	public IEntity searchByName(String name);
}
