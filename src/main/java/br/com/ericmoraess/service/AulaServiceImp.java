package br.com.ericmoraess.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ericmoraess.entity.Aula;
import br.com.ericmoraess.entity.IEntity;
import br.com.ericmoraess.repository.AulaRepository;

@Service
@Transactional
public class AulaServiceImp implements AulaService {
	@Autowired
	private AulaRepository repository;
	
	@Override
	public <T> List<T> findAll() {
		return (List<T>) repository.findAll();
	}

	@Override
	public <T> Optional<T> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends T, T> S save(Object entity) {
		Aula m = (Aula) entity;
		repository.save(m);
		return (S) m;
	}

	@Override
	public <T> void delete(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public IEntity searchByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
