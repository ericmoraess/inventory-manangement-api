package br.com.ericmoraess.service.image;

public interface IGeneratorQRCode {
	public void generateQRCodeImage(String text, int width, int height, String filePath);
}
