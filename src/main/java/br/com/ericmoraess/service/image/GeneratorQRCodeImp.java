package br.com.ericmoraess.service.image;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

@Service
public class GeneratorQRCodeImp implements IGeneratorQRCode {
	
	@Override
	public void generateQRCodeImage(String text, int width, int height, String filePath) {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix;
		try {
			bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
			Path path = FileSystems.getDefault().getPath(filePath);
			MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
		} catch (WriterException | IOException e) {
		}


	}

}
