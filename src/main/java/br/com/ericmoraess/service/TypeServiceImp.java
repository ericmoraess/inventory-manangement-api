package br.com.ericmoraess.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ericmoraess.entity.IEntity;
import br.com.ericmoraess.entity.Type;
import br.com.ericmoraess.repository.TypeRepository;

@Service
@Transactional
public class TypeServiceImp implements TypeService {

	@Autowired
	TypeRepository repository;

	@Override
	public List<Type> findAll() {
		return repository.findAll();
	}

	@Override
	public Optional<Type> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public <S extends T, T> S save(Object entity) {
		Type t = (Type) entity;
		repository.save(t);
		return (S) t;
	}

	@Override
	public <T> void delete(T entity) {
		repository.delete((Type) entity);
	}

	@Override
	public IEntity searchByName(String name) {
		return repository.searchByName(name);
	}
}
