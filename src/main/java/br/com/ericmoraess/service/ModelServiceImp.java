package br.com.ericmoraess.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ericmoraess.entity.Model;
import br.com.ericmoraess.repository.ModelRepository;

@Service
@Transactional
public class ModelServiceImp implements ModelService {
	
	@Autowired
	private ModelRepository repository;

	@Override
	public <T> List<T> findAll() {
		return (List<T>) repository.findAll();
	}

	@Override
	public <T> Optional<T> findById(Long id) {
		return (Optional<T>) repository.findById(id);
	}

	@Override
	public <T> void delete(T entity) {
		repository.delete((Model) entity);
	}

	@Override
	public <S extends T, T> S save(Object entity) {
		Model m = (Model) entity;
		
		repository.save(m);
		
		return (S) m;
	}

	@Override
	public Model searchByName(String name) {
		return repository.searchByName(name);
	}
}
