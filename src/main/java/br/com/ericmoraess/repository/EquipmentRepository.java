package br.com.ericmoraess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ericmoraess.entity.Equipment;

@Repository
public interface EquipmentRepository extends JpaRepository<Equipment, Long> {}