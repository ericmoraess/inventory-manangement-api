package br.com.ericmoraess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ericmoraess.entity.Model;

@Repository
public interface ModelRepository extends JpaRepository<Model, Long> {
	@Query("SELECT m FROM Model m WHERE m.name LIKE ?1")
	public Model searchByName(String name);
	
} 
