package br.com.ericmoraess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ericmoraess.entity.Aula;

@Repository
public interface AulaRepository extends JpaRepository <Aula	, Long>  {

}
