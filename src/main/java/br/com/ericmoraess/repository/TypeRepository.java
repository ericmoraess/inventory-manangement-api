package br.com.ericmoraess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ericmoraess.entity.Type;

@Repository
public interface TypeRepository extends JpaRepository<Type, Long> {
	@Query("SELECT t FROM Type t WHERE t.name LIKE ?1")
	public Type searchByName(String name);
}