package br.com.ericmoraess.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ericmoraess.config.GroupedApiResponsesAvecCreated;
import br.com.ericmoraess.config.GroupedApiResponsesAvecOk;
import br.com.ericmoraess.entity.Type;
import br.com.ericmoraess.exception.ResourceNotFoundException;
import br.com.ericmoraess.service.IServiceCRUD;
import br.com.ericmoraess.service.TypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping({ "/api/v1" })
@Api(value = "Type of equipment entity")
@CrossOrigin (origins = "*")
public class TypeAPI {
	private IServiceCRUD service;

	@Autowired
	public TypeAPI(TypeService pTypeService) {
		service = pTypeService;
	}

	@ApiOperation(value = "get all types registered", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GroupedApiResponsesAvecOk
	@GetMapping(path = "/types", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Type>> getAll() {
		return ResponseEntity.ok(service.findAll());
	}

	@ApiOperation(value = "Get an type by Id", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiParam(value = "Type id from which type object will retrieve", required = true)
	@GroupedApiResponsesAvecOk
	@GetMapping(path = "/types/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Type> findById(@PathVariable(value = "id") Long typeId) throws ResourceNotFoundException {

		Type type = (Type) service.findById(typeId)
				.orElseThrow(() -> new ResourceNotFoundException("Type not found for this id :: " + typeId));

		return ResponseEntity.ok().body(type);

	}
	
	@ApiOperation(value = "Add an type", response = Type.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GroupedApiResponsesAvecCreated
	@PostMapping(path = "/types", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Type> create(@Valid @RequestBody Type type) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(type));
	}
	
	@ApiOperation(value = "Update an type", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PutMapping(path = "/types/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Type> update(@PathVariable(value = "id") Long typeId, @Valid @RequestBody Type typeDetails) throws ResourceNotFoundException {
	
		Type type = (Type) service.findById(typeId).orElseThrow(() -> new ResourceNotFoundException("Type not found for this id :: " + typeId));
			
		type.setId(typeId);
		type.setCode(typeDetails.getCode());
		type.setModels( typeDetails.getModels() );
		type.setName(typeDetails.getName());
		
		final Type updatedEmployee = service.save(type);
		
		return ResponseEntity.ok(updatedEmployee);
	
	}
	
	@ApiOperation(value = "Delete an type", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@DeleteMapping(path = "/types/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long typeId)
	throws ResourceNotFoundException {

		Type type = (Type) service.findById(typeId).orElseThrow(() -> new ResourceNotFoundException("Type not found for this id :: " + typeId));
		service.delete(type);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);

		return response;

	}
}
