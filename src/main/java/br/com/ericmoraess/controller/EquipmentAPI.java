package br.com.ericmoraess.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.ericmoraess.config.GroupedApiResponsesAvecCreated;
import br.com.ericmoraess.config.GroupedApiResponsesAvecOk;
import br.com.ericmoraess.entity.Equipment;
import br.com.ericmoraess.entity.IEntity;
import br.com.ericmoraess.entity.Model;
import br.com.ericmoraess.entity.Type;
import br.com.ericmoraess.exception.ResourceNotFoundException;
import br.com.ericmoraess.service.EquipmentService;
import br.com.ericmoraess.service.IServiceCRUD;
import br.com.ericmoraess.service.ModelService;
import br.com.ericmoraess.service.TypeService;
import br.com.ericmoraess.service.business.ServiceBussines;
import br.com.ericmoraess.service.image.IGeneratorQRCode;
import br.com.ericmoraess.service.storage.IStorage;
import br.com.ericmoraess.service.storage.LocalFileStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping({ "/api/v1" })
@Api(value = "Equipment entity")
@CrossOrigin(origins = "*")
public class EquipmentAPI {

	private IServiceCRUD equipmentService;
	private IServiceCRUD typeService;
	private IServiceCRUD modelService;
	private IStorage fileStorageService;
	private ServiceBussines serviceBusines;
	private IGeneratorQRCode qrCodeGenerator;

	@Autowired
	public EquipmentAPI(EquipmentService pEquipmentService, TypeService pTypeService, ModelService pModelService, LocalFileStorageService pFileStorageService, ServiceBussines pServiceBusines, IGeneratorQRCode pIGeneratorQRCode) {
		equipmentService = pEquipmentService;
		typeService = pTypeService;
		modelService = pModelService;
		fileStorageService = pFileStorageService;
		serviceBusines = pServiceBusines;
		qrCodeGenerator = pIGeneratorQRCode;
	}

	@ApiOperation(value = "get all equipments registered", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GroupedApiResponsesAvecOk
	@GetMapping(path = "/equipments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Equipment>> getAll() {
		return ResponseEntity.ok(equipmentService.findAll());
	}

	@ApiOperation(value = "Get an equipment by Id", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiParam(value = "Equipment id from which equipment object will retrieve", required = true)
	@GroupedApiResponsesAvecOk
	@GetMapping(path = "/equipments/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Equipment> findById(@PathVariable(value = "id") Long equipmentId)
			throws ResourceNotFoundException {

		IEntity equipment = (Equipment) equipmentService.findById(equipmentId)
				.orElseThrow(() -> new ResourceNotFoundException("Equipment not found for this id :: " + equipmentId));
		
		//apply depreciation on current value
		equipment = serviceBusines.apply(equipment);
				
		return ResponseEntity.ok().body((Equipment)equipment);

	}

	@ApiOperation(value = "Add an equipment", response = Equipment.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@GroupedApiResponsesAvecCreated
	@PostMapping(path = "/equipments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<@Valid Equipment> create(@RequestPart(required = true) Equipment equipment,
			@RequestPart(required = true) MultipartFile foto) {

		Model m = (Model) getAttatchedEntity(equipment.getModel(), modelService);
		Type t = (Type) getAttatchedEntity(equipment.getType(), typeService);

		equipment.setModel(m);
		equipment.setType(t);
		
		//generate QRCODE
		String pathLocal = ".//equipQRCode.png";
		genQRCode(equipment, pathLocal);
		
		//Location anywhere in the web
		String fileDownloadUri = fileStorageService.storeToFile(foto);
    		
        equipment.setUrl(fileDownloadUri);
        
        //store with uploaded photo
        equipment = equipmentService.save(equipment);
        
        //overwrite the photo with qrcode
        equipment.setUrl(pathLocal);
        
		return ResponseEntity.status(HttpStatus.CREATED).body(equipment);
	}

	private void genQRCode(Equipment equipment, String pathLocal) {
		// Java object to JSON string
        String jsonString = "";
    	try {
    		ObjectMapper mapper = new ObjectMapper();
			jsonString = mapper.writeValueAsString(equipment);
			System.out.println(jsonString);
		} catch (JsonProcessingException e) {
			System.out.println("Nao foi possivel converter para JSON");
		}
        
    	qrCodeGenerator.generateQRCodeImage(jsonString, 32, 32, pathLocal);
	}

	private IEntity getAttatchedEntity(IEntity e, IServiceCRUD service) {		IEntity attatched = ((IServiceCRUD) service).searchByName(e.getName());

		if (attatched == null)
			attatched = ((IServiceCRUD) service).save(e);

		return attatched;
	}

	@ApiOperation(value = "Update an equipment", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PutMapping(path = "/equipments/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Equipment> update(@PathVariable(value = "id") Long equipmentId,
			@Valid @RequestBody Equipment equipmentDetails) throws ResourceNotFoundException {

		Equipment equipment = (Equipment) equipmentService.findById(equipmentId)
				.orElseThrow(() -> new ResourceNotFoundException("Equipment not found for this id :: " + equipmentId));

		equipment.setId(equipmentId);

		final Equipment updatedEquipment = equipmentService.save(equipment);

		return ResponseEntity.ok(updatedEquipment);

	}

	@ApiOperation(value = "Delete an equipment", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@DeleteMapping(path = "/equipments/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long equipmentId) throws ResourceNotFoundException {

		Equipment equipment = (Equipment) equipmentService.findById(equipmentId)
				.orElseThrow(() -> new ResourceNotFoundException("Equipment not found for this id :: " + equipmentId));
		equipmentService.delete(equipment);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);

		return response;

	}
}
