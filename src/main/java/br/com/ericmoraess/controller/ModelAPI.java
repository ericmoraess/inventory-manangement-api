package br.com.ericmoraess.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ericmoraess.config.GroupedApiResponsesAvecCreated;
import br.com.ericmoraess.config.GroupedApiResponsesAvecOk;
import br.com.ericmoraess.entity.Model;
import br.com.ericmoraess.exception.ResourceNotFoundException;
import br.com.ericmoraess.service.ModelService;
import br.com.ericmoraess.service.IServiceCRUD;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping({ "/api/v1" })
@Api(value = "Model of equipment entity")
@CrossOrigin (origins = "*")
public class ModelAPI {
	private IServiceCRUD service;

	@Autowired
	public ModelAPI(ModelService pModelService) {
		service = pModelService;
	}

	@ApiOperation(value = "get all types registered", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GroupedApiResponsesAvecOk
	@GetMapping(path = "/models", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Model>> getAll() {
		return ResponseEntity.ok(service.findAll());
	}
	
	@ApiOperation(value = "Get an model by Id", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiParam(value = "Model id from which model object will retrieve", required = true)
	@GroupedApiResponsesAvecOk
	@GetMapping(path = "/models/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Model> findById(@PathVariable(value = "id") Long modelId) throws ResourceNotFoundException {

		Model model = (Model) service.findById(modelId)
				.orElseThrow(() -> new ResourceNotFoundException("Model not found for this id :: " + modelId));

		return ResponseEntity.ok().body(model);

	}
	
	@ApiOperation(value = "Add an model", response = Model.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GroupedApiResponsesAvecCreated
	@PostMapping(path = "/models", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<@Valid Model> create(@Valid @RequestBody Model model) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(model));
	}
	
	@ApiOperation(value = "Update an model", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PutMapping(path = "/models/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Model> update(@PathVariable(value = "id") Long modelId, @Valid @RequestBody Model modelDetails) throws ResourceNotFoundException {
	
		Model model = (Model) service.findById(modelId).orElseThrow(() -> new ResourceNotFoundException("Model not found for this id :: " + modelId));
			
		model.setId(modelId);
		model.setCode(modelDetails.getCode());
		model.setName(modelDetails.getName());
		
		final Model updatedEmployee = service.save(model);
		
		return ResponseEntity.ok(updatedEmployee);
	
	}
	
	@ApiOperation(value = "Delete an model", response = ResponseEntity.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@DeleteMapping(path = "/models/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long modelId)
	throws ResourceNotFoundException {

		Model model = (Model) service.findById(modelId).orElseThrow(() -> new ResourceNotFoundException("Model not found for this id :: " + modelId));
		service.delete(model);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);

		return response;

	}
}

