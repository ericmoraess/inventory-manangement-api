package br.com.ericmoraess.entity;


import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.ericmoraess.config.LocalDateTimeDeserializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "equipment")
@ApiModel(description = "All details about the Equipment.")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Equipment implements IEntity, Serializable {

	private static final long serialVersionUID = -6896297454526135752L;

	@ApiModelProperty(notes = "The database generated equipment ID")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private Long id;

	@ApiModelProperty(notes = "Equipment Acquisition Date")
	@Column(name = "acquisitiondate", nullable = false)
	@JsonProperty("acquisitiondate")
	@JsonFormat(pattern="MM-yyyy")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDate acquisitionDate;

	@ApiModelProperty(notes = "Equipment code identifier")
	@Column(name = "code", nullable = true)
	@JsonProperty("code")
	private String code;
	
	@ApiModelProperty(notes = "Aquisition value")
	@Column(name = "valueacquisition", nullable = true)
	@JsonFormat(pattern="yyyy-MM-dd")
	@JsonProperty("valueacquisition")
	private Double valueAcquisition;
	
	@Transient
	@JsonProperty("depreciatedvalue")
	private Double depreciatedValue;
	
	@ApiModelProperty(notes = "URL image equipment")
	@Column(name = "url", nullable = true)
	@JsonFormat(pattern="yyyy-MM-dd")
	@JsonProperty("url")
	private String url;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	@ManyToOne(cascade =   CascadeType.PERSIST, targetEntity = Type.class, fetch = FetchType.LAZY )
	@JoinColumn(name = "type_id", referencedColumnName = "id")
	private Type type;
	
	@ManyToOne(cascade =   CascadeType.PERSIST, targetEntity = Model.class, fetch = FetchType.LAZY )
	@JoinColumn(name = "model_id", referencedColumnName = "id")
	private Model model;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public LocalDate getAcquisitionDate() {
		return acquisitionDate;
	}

	public void setAcquisitionDate(LocalDate acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}

	public Double getValueAcquisition() {
		return valueAcquisition;
	}

	public void setValueAcquisition(Double valueAcquisition) {
		this.valueAcquisition = valueAcquisition;
	}

	public Double getDepreciatedValue() {
		return depreciatedValue;
	}

	public void setDepreciatedValue(Double depreciatedValue) {
		this.depreciatedValue = depreciatedValue;
	}

	@Override
	public String toString() {
		return "Equipment [id=" + id + ", acquisitionDate=" + acquisitionDate + ", code=" + code + ", valueAcquisition="
				+ valueAcquisition + ", depreciatedValue=" + depreciatedValue + ", url=" + url + ", type=" + type
				+ ", model=" + model + "]";
	}
	
}
