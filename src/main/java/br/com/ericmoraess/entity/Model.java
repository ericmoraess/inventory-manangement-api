package br.com.ericmoraess.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity(name = "Model")
@Table(name = "model")
@ApiModel(description = "All details about the Model. ")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Model implements br.com.ericmoraess.entity.IEntity, Serializable {

	private static final long serialVersionUID = 4833291620516994538L;

	@ManyToOne
	@JoinColumn(name = "type_id")
	@JsonBackReference
	private Type type;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@ApiModelProperty(notes = "The database generated model ID")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private Long id;

	@ApiModelProperty(notes = "Model name")
	@Column(name = "name", nullable = false, unique = true)
	@JsonProperty("name")
	private String name;

	@ApiModelProperty(notes = "Model code identifier")
	@Column(name = "code", nullable = false, unique = true)
	@JsonProperty("code")
	private String code;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toUpperCase();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code.toUpperCase();
	}

	@Override
	public String toString() {
		return "Model [id=" + id + ", name=" + name + ", code=" + code + "]";
	}
}
