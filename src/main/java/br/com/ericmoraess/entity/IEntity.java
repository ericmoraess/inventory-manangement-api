package br.com.ericmoraess.entity;

public interface IEntity {
	public String getName();
	public Long getId();
}
