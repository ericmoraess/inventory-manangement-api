package br.com.ericmoraess.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity(name = "Type")
@Table(name = "type")
@ApiModel(description = "All details about the Type of equipment.")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Type implements br.com.ericmoraess.entity.IEntity, Serializable {

	private static final long serialVersionUID = 7034567178450273965L;

	@OneToMany(mappedBy = "type", targetEntity = Model.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Model> models;

	public List<Model> getModels() {
		return models;
	}

	public void setModels(List<Model> models) {
		this.models = models;
	}

	@ApiModelProperty(notes = "The database generated type ID")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private Long id;

	@ApiModelProperty(notes = "Type name")
	@Column(name = "name", nullable = false, unique = true)
	@JsonProperty("name")
	private String name;

	@ApiModelProperty(notes = "Type code identifier")
	@Column(name = "code", nullable = false, unique = true)
	@JsonProperty("code")
	private String code;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toUpperCase();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code.toUpperCase();
	}

	@Override
	public String toString() {
		return "Type [models=" + models + ", id=" + id + ", name=" + name + ", code=" + code + "]";
	}
}