package br.com.ericmoraess.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "inventory")
@ApiModel(description="All details about the Inventory. ")
public class Inventory {

	@ApiModelProperty(notes = "The database generated inventory ID")
	private long id;

	@ApiModelProperty(notes = "Inventory Creation Date")
	private Date creationDate;

	@ApiModelProperty(notes = "Inventory code identifier")
	private String code;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "creation_date", nullable = false)
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name = "code", nullable = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Inventory [id=" + id + ", creationDate=" + creationDate + ", code=" + code + "]";
	}
}

