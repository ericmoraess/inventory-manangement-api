package br.com.ericmoraess.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src/test/java/resources/features/user.feature", 
	plugin = {"pretty", "html:target/cucumber"},
	glue = "br.com.ericmoraess.steps")

public class IMAPICucmberIntegrationTest {

}
