package br.com.ericmoraess.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.ericmoraess.rest.tests.TypeAPIRestTest;
import cucumber.api.java8.En;

public class TypeAPISteps  extends TypeAPIRestTest implements En {

	private final Logger log = LoggerFactory.getLogger(TypeAPISteps.class);

	public TypeAPISteps() {
		
		Given("que nao ha nenhum tipo de equipamento cadastrado", () -> {
		   
		});

		When("quando eu recuperar todos cadastrados", () -> {
		   
		});

		Then("devo receber uma resposta vazia", () -> {
		   
		});
		

	}
}
