Feature: Validar os parâmetros do job processo protesto 
	Como usuário
	Eu quero informar a data de recepção para o job de protesto
	Para que o job processe pela data de recepção

Scenario: Putting one thing in the bag
		Given the bag is empty
		When I put 1 potato in the bag
		Then the bag should contain only 1 potato

Scenario: Putting few things in the bag
		Given the bag is empty
		When I put 1 potato in the bag
		And I put 2 cucumber in the bag
		Then the bag should contain 1 potato
		And the bag should contain 2 cucumber
