FROM maven:3.5-jdk-8

WORKDIR /usr/src/app

COPY ./API /usr/src/app

RUN mvn --version

RUN mvn -Dmaven.test.skip=true package

ENV PORT=9090
EXPOSE $PORT
